// Soal No. 1 (Range)
function range(startNum, finishNum) {
  let number = [];
  if (startNum == null || finishNum == null) return (number = -1);
  if (startNum < finishNum) {
    let newNumber = startNum - 1;
    for (let i = startNum; i <= finishNum; i++) {
      newNumber = newNumber + 1;
      number.push(newNumber);
    }
    return number;
  } else if (startNum > finishNum) {
    let newNumber = finishNum - 1;
    for (let i = finishNum; i <= startNum; i++) {
      newNumber = newNumber + 1;
      number.push(newNumber);
    }
    return number.reverse();
  }
}
console.log(range(1, 10)); //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)); // -1
console.log(range(11, 18)); // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)); // [54, 53, 52, 51, 50]
console.log(range()); // -1

// Soal No. 2 (Range with Step)
function rangeWithStep(startNum, finishNum, step) {
  let number = [];
  if (startNum == null || finishNum == null) return (number = -1);
  if (startNum < finishNum) {
    let newNumber = startNum;
    for (let i = startNum; i <= finishNum; ) {
      number.push(newNumber);
      newNumber = newNumber + step;
      i = i + step;
    }
    return number;
  } else if (startNum > finishNum) {
    let newNumber = finishNum - 1;
    for (let i = finishNum; i <= startNum; ) {
      newNumber = newNumber + step;
      number.push(newNumber);
      i = i + step;
    }
    return number.reverse();
  }
}

console.log(rangeWithStep(1, 10, 2)); // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)); // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)); // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)); // [29, 25, 21, 17, 13, 9, 5]

// Soal No. 3 (Sum of Range)
function sum(startNum, finishNum, step) {
  if (step == null) step = 1;
  let number = 0;
  if (startNum == null && finishNum == null) return (number = 0);
  if (finishNum == null) return (number = startNum);
  if (startNum < finishNum) {
    let newNumber = startNum;
    for (let i = startNum; i <= finishNum; ) {
      number = number + newNumber;
      newNumber = newNumber + step;
      i = i + step;
    }
    return number;
  } else if (startNum > finishNum) {
    let newNumber = startNum;
    for (let i = startNum; i >= finishNum; ) {
      number = number + newNumber;
      newNumber = newNumber - step;
      i = i - step;
    }
    return number;
  }
}

console.log(sum(1, 10)); // 55
console.log(sum(5, 50, 2)); // 621
console.log(sum(15, 10)); // 75
console.log(sum(20, 10, 2)); // 90
console.log(sum(1)); // 1
console.log(sum()); // 0

// Soal No. 4 (Array Multidimensi)
var input = [
  ['0001', 'Roman Alamsyah', 'Bandar Lampung', '21/05/1989', 'Membaca'],
  ['0002', 'Dika Sembiring', 'Medan', '10/10/1992', 'Bermain Gitar'],
  ['0003', 'Winona', 'Ambon', '25/12/1965', 'Memasak'],
  ['0004', 'Bintang Senjaya', 'Martapura', '6/4/1970', 'Berkebun'],
];

function dataHandling() {
  for (let i = 0; input[i]; i++) {
    console.log('Nomor ID: ', input[i][0]);
    console.log('Nama Lengkap: ', input[i][1]);
    console.log('TTL: ', input[i][2]);
    console.log('Hobi: ', input[i][3]);
  }
}

dataHandling();

// Soal No. 5 (Balik Kata)
function balikKata(kata) {
  return kata.split('').reverse().join('');
}

console.log(balikKata('Kasur Rusak')); // kasuR rusaK
console.log(balikKata('SanberCode')); // edoCrebnaS
console.log(balikKata('Haji Ijah')); // hajI ijaH
console.log(balikKata('racecar')); // racecar
console.log(balikKata('I am Sanbers')); // srebnaS ma I

// Soal No. 6 (Metode Array)
function dataHandling2(kata2) {
  kata2.splice(1, 2);
  kata2.splice(1, 0, 'Roman Alamsyah Elsharawy', 'Provinsi Bandar Lampung');
  kata2.splice(4, 1);
  kata2.splice(4, 0, 'Pria', 'SMA Internasional Metro');
  console.log(kata2);
  let bulan = parseInt(kata2[3].split('/')[1]);
  switch (bulan) {
    case 1:
      console.log('Januari');
      break;
    case 2:
      console.log('Februari');
      break;
    case 3:
      console.log('Maret');
      break;
    case 4:
      console.log('April');
      break;
    case 5:
      console.log('Mei');
      break;
    case 6:
      console.log('Juni');
      break;
    case 7:
      console.log('Juli');
      break;
    case 8:
      console.log('Agustus');
      break;
    case 9:
      console.log('September');
      break;
    case 10:
      console.log('Oktober');
      break;
    case 11:
      console.log('November');
      break;
    case 12:
      console.log('Desember');
      break;
    default:
      break;
  }
  let = tanggalBaru = kata2[3].split('/').join('-');
  console.log(tanggalBaru);
  maksKata = kata2[1].split('').slice(0, 15).join('');
  console.log(maksKata);
}

var input = [
  '0001',
  'Roman Alamsyah ',
  'Bandar Lampung',
  '21/05/1989',
  'Membaca',
];
dataHandling2(input);

/**
 * keluaran yang diharapkan (pada console)
 *
 * ["0001", "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung", "21/05/1989", "Pria", "SMA Internasional Metro"]
 * Mei
 * ["1989", "21", "05"]
 * 21-05-1989
 * Roman Alamsyah
 */
