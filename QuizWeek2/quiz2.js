/* 
  SOAL CLASS SCORE (15 poin + 5 es6)
  Buatlah sebuah class dengan nama Score. class Score tersebut memiliki properti "subject", "points", dan "email". 
  "points" dapat di input berupa number atau array of number.
  tambahkan method average untuk menghitung rata-rata dari parameter points ketika yang di input berupa array (lebih dari 1 nilai)
*/
//tuliskan coding disini

class Score {
  constructor(subject, points, email) {
    this._subject = subject;
    this._points = points;
    this._email = email;
  }
  get average() {
    let total = 0;
    for (let i = 0; i < this._points.length; i++) {
      total += this._points[i];
    }
    let avg = total / grades.length;
    return avg;
  }
}

//end

/* 
  SOAL View Score (15 Poin + 5 Poin ES6)
  Membuat function viewScores yang menerima parameter data berupa array multidimensi dan subject berupa string
  Function viewScores mengolah data email dan nilai skor pada parameter array 
  lalu mengembalikan data array yang berisi object yang dibuat dari class Score.
  Contoh: 
  Input 

  data : 
  [
    ["email", "quiz-1", "quiz-2", "quiz-3"],
    ["abduh@mail.com", 78, 89, 90],
    ["khairun@mail.com", 95, 85, 88]
  ]

  subject: "quiz-1"

  Output 
  [
    {email: "abduh@mail.com", subject: "quiz-1", points: 78},
    {email: "khairun@mail.com", subject: "quiz-1", points: 95},
  ]
*/
//tuliskan coding disini

let data = [
  ['email', 'quiz-1', 'quiz-2', 'quiz-3'],
  ['abduh@mail.com', 78, 89, 90],
  ['khairun@mail.com', 95, 85, 88],
];
let score = [];
let viewScores = (data, quiz) => {
  let firstData = data[0];
  let x, y, z;

  for (let j = 0; j < firstData.length; j++) {
    if (firstData[j] == quiz) {
      x = j;
    }
  }

  let { dataEmail, dataQuiz1, dataQuiz2, dataQuiz3 } = data[0];
  for (let i = 1; i < data.length; i++) {
    let data3 = data[i];
    let obj = {
      dataEmail: data3[0],
      subject: quiz,
      points: data3[x],
    };
    console.log(obj);
  }
};

//end
//TEST CASE
viewScores(data, 'quiz-1');
viewScores(data, 'quiz-2');
viewScores(data, 'quiz-3');

/* 
  SOAL Recap Score (20 Poin + 5 Poin ES6)
    buatlah function recapScore menampilkan perolehan nilai semua student. 
    Data yang ditampilkan adalah email user, nilai rata-rata, dan predikat kelulusan. 
    Predikat kelulusan ditentukan dari aturan berikut:
    nilai > 70 "participant"
    nilai > 80 "graduate"
    nilai > 90 "honour"

    contoh output:
    1. Email: abduh@mail.com
    Rata-rata: 85.7
    Predikat: graduate

    2. Email: khairun@mail.com
    Rata-rata: 89.3
    Predikat: graduate

    3. Email: bondra@mail.com
    Rata-rata: 74.3
    Predikat: participant

    4. Email: regi@mail.com
    Rata-rata: 91
    Predikat: honour

*/
//tuliskan code anda disini

let newData = [
  ['abduh@mail.com', 85.7],
  ['khairun@mail.com', 89.3],
  ['bondra@mail.com', 74.3],
  ['regi@mail.com', 91],
];

let recapScores = (data) => {
  let mention;
  let output = [];
  for (let i = 0; i < data.length; i++) {
    let score = data[i];
    if (score[1] > 70) {
      mention = 'participant';
    }
    if (score[1] > 80) {
      mention = 'graduate';
    }
    if (score[1] > 90) {
      mention = 'honour';
    }
    let obj = {
      Email: score[0],
      'Rata-rata': score[1],
      predikat: mention,
    };
    output.push(obj);
  }
  console.log(output);
};

//end
// DRIVEN CODE
recapScores(newData);
