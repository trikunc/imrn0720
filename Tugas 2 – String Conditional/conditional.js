// Tugas if-else
var nama = prompt('Masukan nama: ');
if (nama.length > 0) {
  if (peran.length > 0 && peran == 'Penyihir') {
    alert('Selamat datang di Dunia Werewolf, ' + nama);
    alert(
      'Halo ' +
        peran +
        ' ' +
        nama +
        ', kamu dapat melihat siapa yang menjadi werewolf!'
    );
  } else if (peran.length > 0 && peran == 'Guard') {
    alert('Selamat datang di Dunia Werewolf, ' + nama);
    alert(
      'Halo ' +
        peran +
        ' ' +
        nama +
        ', kamu akan membantu melindungi temanmu dari serangan werewolf'
    );
  } else if (peran.length > 0 && peran == 'Werewolf') {
    alert('Selamat datang di Dunia Werewolf, ' + nama);
    alert(
      'Halo ' + peran + ' ' + nama + ', Kamu akan memakan mangsa setiap malam!'
    );
  } else {
    alert('Halo ' + nama + ', Pilih peranmu untuk memulai game!');
  }
} else {
  alert('Nama harus diisi!');
}

// Tugas Switch Case
var hari = prompt('Masukan hari (1 - 31): ');
var bulan = prompt('Masukan bulan (1 - 12): ');
var tahun = prompt('Masukan tahun (1900 - 2200): ');

switch (parseInt(bulan)) {
  case 1:
    bulan = 'Januari';
    break;
  case 2:
    bulan = 'Februari';
    break;
  case 3:
    bulan = 'Maret';
    break;
  case 4:
    bulan = 'April';
    break;
  case 5:
    bulan = 'Mei';
    break;
  case 6:
    bulan = 'Juni';
    break;
  case 7:
    bulan = 'Juli';
    break;
  case 8:
    bulan = 'Agustus';
    break;
  case 9:
    bulan = 'September';
    break;
  case 10:
    bulan = 'Oktober';
    break;
  case 11:
    bulan = 'November';
    break;
  case 12:
    bulan = 'Desember';
}

console.log(`${hari} ${bulan} ${tahun}`);
