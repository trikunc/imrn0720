import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import FontIcon from 'react-native-vector-icons/FontAwesome';

const LoginScreen = () => {
  return (
    <View>
      <View style={styles.profile}>
        <Text style={styles.aboutText}>Tentang Saya</Text>
        <Icon
          style={styles.photoIcon}
          name="account-circle"
          color="#888888"
          size={160}
        />
        <Text style={styles.nameText}>Septiana Trikuncoro</Text>
        <Text style={styles.devText}>React Native Developer</Text>
      </View>
      <View style={styles.portfolio}>
        <View>
          <Text style={styles.titleText}>Portfolio</Text>
          <View style={{height: 1, backgroundColor: '#003366'}} />
          <View></View>
          <View style={styles.portBar}>
            <View style={styles.gitBar}>
              <Text>
                <FontIcon name="gitlab" size={35} color="#3EC6FF" />;
              </Text>
              <Text style={styles.nameUser}>@trikunc</Text>
            </View>
            <View style={styles.gitBar}>
              <Text>
                <FontIcon name="github" size={35} color="#3EC6FF" />;
              </Text>
              <Text style={styles.nameUser}>@trikunc</Text>
            </View>
          </View>
        </View>
      </View>
      <View style={styles.portfolio}>
        <View>
          <Text style={styles.titleText}>Hubungi Saya</Text>
          <View style={{height: 1, backgroundColor: '#003366'}} />
          <View></View>
          <View style={styles.portBarV}>
            <View style={styles.gitBarV}>
              <Text style={{marginRight: 10}}>
                <FontIcon name="facebook-square" size={35} color="#3EC6FF" />;
              </Text>
              <Text style={styles.nameUser}>@trikunc</Text>
            </View>
          </View>
          <View style={styles.portBarV}>
            <View style={styles.gitBarV}>
              <Text style={{marginRight: 10}}>
                <FontIcon name="instagram" size={35} color="#3EC6FF" />;
              </Text>
              <Text style={styles.nameUser}>@trikunc</Text>
            </View>
          </View>
          <View style={styles.portBarV}>
            <View style={styles.gitBarV}>
              <Text style={{marginRight: 10}}>
                <FontIcon name="twitter" size={35} color="#3EC6FF" />;
              </Text>
              <Text style={styles.nameUser}>@trikunc</Text>
            </View>
          </View>
        </View>
      </View>
    </View>
  );
};

export default LoginScreen;

const styles = StyleSheet.create({
  profile: {
    alignItems: 'center',
    marginTop: 14,
  },
  aboutText: {
    fontFamily: 'Roboto',
    fontStyle: 'normal',
    fontWeight: 'bold',
    fontSize: 30,
    color: '#003366',
  },
  photoIcon: {
    marginVertical: 2,
  },
  nameText: {
    fontFamily: 'Roboto',
    fontStyle: 'normal',
    fontWeight: 'bold',
    fontSize: 20,
    color: '#003366',
  },
  devText: {
    fontFamily: 'Roboto',
    fontStyle: 'normal',
    fontWeight: 'bold',
    fontSize: 12,
    color: '#3EC6FF',
  },
  portfolio: {
    marginTop: 6,
    backgroundColor: '#e5e5e5',
    borderRadius: 16,
    paddingHorizontal: 8,
    paddingVertical: 4,
  },
  titleText: {
    fontFamily: 'Roboto',
    fontStyle: 'normal',
    fontWeight: 'normal',
    fontSize: 18,
    lineHeight: 21,
    color: '#003366',
  },
  portBar: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingTop: 16,
    paddingHorizontal: 50,
  },
  gitBar: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  nameUser: {
    marginTop: 3,
    marginBottom: 8,
    fontFamily: 'Roboto',
    fontStyle: 'normal',
    fontWeight: 'bold',
    fontSize: 16,
    lineHeight: 19,
    color: '#003366',
  },
  portBarV: {
    alignItems: 'center',
    paddingTop: 16,
    paddingHorizontal: 50,
  },
  gitBarV: {
    flexDirection: 'row',
  },
});
