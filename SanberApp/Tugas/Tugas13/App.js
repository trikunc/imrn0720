import React from 'react';
import {View} from 'react-native';
import Login from './components/LoginScreen';
import About from './components/AboutScreen';

const App = () => {
  return (
    <View>
      {/* <Login /> */}
      <About />
    </View>
  );
};

export default App;
