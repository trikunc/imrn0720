import React from 'react';
// import Main from './components/Main';
import SkillScreen from './SkillScreen';
import {View} from 'react-native';

export default class App extends React.Component {
  render() {
    return (
      <View>
        {/* <Main /> */}
        <SkillScreen />
      </View>
    );
  }
}
