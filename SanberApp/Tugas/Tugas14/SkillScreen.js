import React from 'react';
import {
  StyleSheet,
  Image,
  View,
  Text,
  TouchableOpacity,
  FlatList,
  Alert,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import skillData from './skillData.json';
import Item from './components/Item';

const App = () => {
  // const renderItem = ({item}) => (
  //   <Item
  //     skillName={item.skillName}
  //     categoryName={item.categoryName}
  //     iconName={item.iconName}
  //     percentageProgress={item.percentageProgress}
  //   />
  // );

  return (
    <View>
      <View style={styles.imgLogo}>
        <Image
          source={require('../Tugas13/images/logo.png')}
          style={{width: 187.5, height: 51}}
        />
      </View>
      <View style={styles.profile}>
        <View style={styles.accountPhoto}>
          <TouchableOpacity>
            <Icon
              style={styles.navItem}
              name="account-circle"
              size={32}
              color={'#3EC6FF'}
            />
          </TouchableOpacity>
        </View>
        <View>
          <Text style={styles.haiText}>Hai</Text>
          <Text style={styles.nameText}>Septiana Trikuncoro</Text>
        </View>
      </View>
      <View style={styles.title}>
        <Text style={styles.skillText}>SKILL</Text>
      </View>
      <View style={styles.line} />

      <View style={styles.menuContainer}>
        <View style={styles.menuList}>
          <Text style={styles.menuText}>Library / Framework</Text>
        </View>
        <View style={styles.menuList}>
          <Text style={styles.menuText}>Bahasa Pemrograman</Text>
        </View>
        <View style={styles.menuList}>
          <Text style={styles.menuText}>Teknologi</Text>
        </View>
      </View>

      <FlatList
        data={skillData.items}
        // renderItem={renderItem}
        renderItem={(DATA) => <Item DATA={DATA.item} />}
        keyExtractor={(item) => item.id.toString()}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  imgLogo: {
    alignSelf: 'flex-end',
  },
  profile: {
    flexDirection: 'row',
    marginTop: 3,
    marginLeft: 19,
  },
  accountPhoto: {
    marginRight: 11,
  },
  haiText: {
    fontFamily: 'Roboto',
    fontStyle: 'normal',
    fontWeight: 'normal',
    fontSize: 12,
    lineHeight: 14,
    color: '#000000',
  },
  nameText: {
    fontFamily: 'Roboto',
    fontStyle: 'normal',
    fontWeight: 'normal',
    fontSize: 16,
    lineHeight: 19,
    color: '#003366',
  },
  title: {
    marginTop: 16,
    marginLeft: 18,
  },
  skillText: {
    fontFamily: 'Roboto',
    fontStyle: 'normal',
    fontWeight: 'normal',
    fontSize: 36,
    lineHeight: 42,
    color: '#003366',
  },
  line: {
    height: 4,
    backgroundColor: '#3EC6FF',
    marginHorizontal: 18,
  },
  menuContainer: {
    marginTop: 10,
    flexDirection: 'row',
    marginHorizontal: 18,
    justifyContent: 'space-between',
  },
  menuList: {
    marginTop: 0,
    backgroundColor: '#B4E9FF',
    paddingVertical: 4,
    padding: 10,
    height: 30,
    borderRadius: 10,
  },
  menuText: {
    fontFamily: 'Roboto',
    fontStyle: 'normal',
    fontWeight: 'bold',
    fontSize: 11,
    lineHeight: 19,
    color: '#003366',
  },
  body: {
    flex: 1,
  },
  containerBody: {
    flexDirection: 'row',
    marginHorizontal: 19,
    marginTop: 10,
    backgroundColor: '#B4E9FF',
    paddingVertical: 4,
    padding: 10,
    height: 130,
    borderRadius: 10,
    justifyContent: 'space-between',
  },
  iconBody: {
    justifyContent: 'center',
  },
  contentBody: {
    padding: 5,
  },
  titleBody: {
    fontFamily: 'Roboto',
    fontStyle: 'normal',
    fontWeight: 'bold',
    fontSize: 24,
    lineHeight: 28,
    color: '#003366',
    margin: 2,
  },
  titleDescBody: {
    fontFamily: 'Roboto',
    fontStyle: 'normal',
    fontWeight: 'bold',
    fontSize: 16,
    lineHeight: 19,
    color: '#3EC6FF',
    margin: 2,
  },
  percentTitleBody: {
    fontFamily: 'Roboto',
    fontStyle: 'normal',
    fontWeight: 'bold',
    fontSize: 48,
    lineHeight: 56,
    color: '#FFFFFF',
    alignSelf: 'flex-end',
    margin: 2,
  },
});

export default App;
