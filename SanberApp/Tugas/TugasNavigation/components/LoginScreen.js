import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  Button,
  Alert,
} from 'react-native';

const ScreenContainer = ({children}) => (
  <View style={styles.container}>{children}</View>
);

const LoginScreen = () => {
  return (
    <ScreenContainer>
      <View style={{marginVertical: 25}}>
        <Image source={require('../images/logo.png')} />
      </View>
      <View style={{alignItems: 'center'}}>
        <Text style={styles.loginText}>Login</Text>
      </View>
      <View style={styles.inputUser}>
        <Text style={styles.titleInput}>Username / Email</Text>
        <TextInput style={styles.inputSquare} />
        <Text style={styles.titleInput}>Username / Email</Text>
        <TextInput style={styles.inputSquare} />
      </View>
      <View style={{alignItems: 'center', marginTop: 22}}>
        <View style={styles.buttonSize}>
          <Button
            title="Masuk"
            onPress={() => Alert.alert('Masuk')}
            style={styles.loginButton}
            textStyle={{color: '#FFFFFF', fontSize: 24}}
          />
        </View>
        <View style={{margin: 10}}>
          <Text>atau</Text>
        </View>
        <View style={styles.buttonSize}>
          <Button
            title="Daftar"
            onPress={() => Alert.alert('Daftar')}
            style={styles.registerButton}
            color="#003366"
          />
        </View>
      </View>
    </ScreenContainer>
  );
};

export default LoginScreen;

const styles = StyleSheet.create({
  loginText: {
    marginTop: 7,
    fontFamily: 'Roboto',
    fontStyle: 'normal',
    fontWeight: 'normal',
    fontSize: 24,
    lineHeight: 28,
    color: '#003366',
  },
  inputUser: {
    marginTop: 12,
    marginHorizontal: 40,
  },
  titleInput: {
    marginTop: 16,
    fontFamily: 'Roboto',
    fontStyle: 'normal',
    fontWeight: 'normal',
    fontSize: 16,
    lineHeight: 19,
    color: '#003366',
  },
  inputSquare: {
    marginTop: 4,
    borderWidth: 1,
    borderColor: '#003366',
  },
  loginButton: {
    backgroundColor: '#3EC6FF',
    borderRadius: 16,
  },
  registerButton: {
    backgroundColor: '#003366',
    borderRadius: 26,
  },
  buttonSize: {
    width: 140,
    height: 40,
    color: 'red',
  },
});
