import React, {Component} from 'react';
import {StyleSheet, Image, View, Text, TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

export default class Item extends Component {
  render() {
    let DATA = this.props.DATA;
    return (
      <View style={styles.body}>
        <View>
          <View style={styles.containerBody}>
            <View style={styles.iconBody}>
              <TouchableOpacity>
                <Icon
                  style={styles.navItem}
                  name={DATA.iconName}
                  size={75}
                  color={'#003366'}
                />
              </TouchableOpacity>
            </View>
            <View style={styles.contentBody}>
              <Text style={styles.titleBody}>{DATA.skillName}</Text>
              <Text style={styles.titleDescBody}>{DATA.categoryName}</Text>
              <Text style={styles.percentTitleBody}>
                {DATA.percentageProgress}
              </Text>
            </View>
            <View style={styles.iconBody}>
              <TouchableOpacity>
                <Icon
                  style={styles.navItem}
                  name="chevron-right"
                  size={75}
                  color={'#003366'}
                />
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  imgLogo: {
    alignSelf: 'flex-end',
  },
  profile: {
    flexDirection: 'row',
    marginTop: 3,
    marginLeft: 19,
  },
  accountPhoto: {
    marginRight: 11,
  },
  haiText: {
    fontFamily: 'Roboto',
    fontStyle: 'normal',
    fontWeight: 'normal',
    fontSize: 12,
    lineHeight: 14,
    color: '#000000',
  },
  nameText: {
    fontFamily: 'Roboto',
    fontStyle: 'normal',
    fontWeight: 'normal',
    fontSize: 16,
    lineHeight: 19,
    color: '#003366',
  },
  title: {
    marginTop: 16,
    marginLeft: 18,
  },
  skillText: {
    fontFamily: 'Roboto',
    fontStyle: 'normal',
    fontWeight: 'normal',
    fontSize: 36,
    lineHeight: 42,
    color: '#003366',
  },
  line: {
    height: 4,
    backgroundColor: '#3EC6FF',
    marginHorizontal: 18,
  },
  menuContainer: {
    marginTop: 10,
    flexDirection: 'row',
    marginHorizontal: 18,
    justifyContent: 'space-between',
  },
  menuList: {
    marginTop: 0,
    backgroundColor: '#B4E9FF',
    paddingVertical: 4,
    padding: 10,
    height: 30,
    borderRadius: 10,
  },
  menuText: {
    fontFamily: 'Roboto',
    fontStyle: 'normal',
    fontWeight: 'bold',
    fontSize: 11,
    lineHeight: 19,
    color: '#003366',
  },
  body: {
    flex: 1,
  },
  containerBody: {
    flexDirection: 'row',
    marginHorizontal: 19,
    marginTop: 10,
    backgroundColor: '#B4E9FF',
    paddingVertical: 4,
    padding: 10,
    height: 130,
    borderRadius: 10,
    justifyContent: 'space-between',
  },
  iconBody: {
    justifyContent: 'center',
  },
  contentBody: {
    padding: 5,
  },
  titleBody: {
    fontFamily: 'Roboto',
    fontStyle: 'normal',
    fontWeight: 'bold',
    fontSize: 24,
    lineHeight: 28,
    color: '#003366',
    margin: 2,
  },
  titleDescBody: {
    fontFamily: 'Roboto',
    fontStyle: 'normal',
    fontWeight: 'bold',
    fontSize: 16,
    lineHeight: 19,
    color: '#3EC6FF',
    margin: 2,
  },
  percentTitleBody: {
    fontFamily: 'Roboto',
    fontStyle: 'normal',
    fontWeight: 'bold',
    fontSize: 48,
    lineHeight: 56,
    color: '#FFFFFF',
    alignSelf: 'flex-end',
    margin: 2,
  },
});
