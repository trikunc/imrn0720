import React from 'react';
import {NavigationContainer, StackActions} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {createDrawerNavigator} from '@react-navigation/drawer';

import Login from './components/LoginScreen';
import Add from './components/AddScreen';
import About from './components/AboutScreen';
import Project from './components/ProjectScreen';
import Skill from './components/SkillScreen';

const Tabs = createBottomTabNavigator();
const Stack = createBottomTabNavigator();
const LoginStack = createStackNavigator();
const AddStack = createStackNavigator();
const AboutStack = createStackNavigator();
const ProjectStack = createStackNavigator();
const SkillStack = createStackNavigator();

const LoginStackScreen = () => (
  <LoginStack.Navigator>
    <LoginStack.Screen name="Login" component={Login} />
  </LoginStack.Navigator>
);

const AddStackScreen = () => (
  <AddStack.Navigator>
    <AddStack.Screen name="Add" component={Add} />
  </AddStack.Navigator>
);

const AboutStackScreen = () => (
  <AboutStack.Navigator>
    <AboutStack.Screen name="About" component={About} />
  </AboutStack.Navigator>
);

const ProjectStackScreen = () => (
  <ProjectStack.Navigator>
    <ProjectStack.Screen name="Project" component={Project} />
  </ProjectStack.Navigator>
);

const SkillStackScreen = () => (
  <SkillStack.Navigator>
    <SkillStack.Screen name="Skill" component={Skill} />
  </SkillStack.Navigator>
);

const TabsScreen = () => (
  <Tabs.Navigator>
    <Tabs.Screen name="Skill" component={SkillStackScreen} />
    <Tabs.Screen name="Project" component={ProjectStackScreen} />
    <Tabs.Screen name="Add" component={AddStackScreen} />
  </Tabs.Navigator>
);

const Drawer = createDrawerNavigator();
const AppDrawer = () => (
  <Drawer.Navigator>
    <Drawer.Screen name="Tab" component={TabsScreen} />
    <Drawer.Screen name="About" component={AboutStackScreen} />
  </Drawer.Navigator>
);

const AppStack = () => (
  <Stack.Navigator>
    <Stack.Screen name="Login" component={LoginStackScreen} />
    <Stack.Screen name="AppDrawer" component={AppDrawer} />
  </Stack.Navigator>
);

export default () => (
  <NavigationContainer>
    <AppStack />
  </NavigationContainer>
);
