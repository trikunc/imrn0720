/**
 * @format
 */

import {AppRegistry} from 'react-native';
// import App from './Tugas/Tugas12/App';
// import App from './Tugas/Tugas13/App';
// import App from './Tugas/Tugas14/App';
// import App from './Tugas/Tugas15/index';
// import App from './Tugas/TugasNavigation/index';
import App from './Quiz3/index';
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => App);
