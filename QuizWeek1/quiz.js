// TEST CASES BalikString
function balikString(string) {
  var len = string.length,
    result = '';
  for (var i = 0; i <= len - 1; i++) {
    result = result + string[len - i - 1];
  }
  return result;
}

console.log(balikString('abcde')); // edcba
console.log(balikString('rusak')); // kasur
console.log(balikString('racecar')); // racecar
console.log(balikString('haji')); // ijah

// TEST CASES Palindrome
function palindrome(string) {
  var len = string.length,
    result = '';
  for (var i = 0; i <= len - 1; i++) {
    result = result + string[len - i - 1];
  }
  if (string === result) {
    return true;
  } else {
    return false;
  }
}

console.log(palindrome('kasur rusak')); // true
console.log(palindrome('haji ijah')); // true
console.log(palindrome('nabasan')); // false
console.log(palindrome('nababan')); // true
console.log(palindrome('jakarta')); // false

// TEST CASES Bandingkan Angka
function bandingkan(num1, num2) {
  if (num1 > 0 && num2 > 0) {
    if (num1 > num2) {
      return num1;
    } else if (num1 < num2) {
      return num2;
    } else {
      return -1;
    }
  } else if (num1 > 0) {
    return num1;
  } else if (num2 > 0) {
    return num2;
  } else {
    return -1;
  }
}

console.log(bandingkan(10, 15)); // 15
console.log(bandingkan(12, 12)); // -1
console.log(bandingkan(-1, 10)); // -1
console.log(bandingkan(112, 121)); // 121
console.log(bandingkan(1)); // 1
console.log(bandingkan()); // -1
console.log(bandingkan('15', '18')); // 18

// TEST CASES Descending Ten
function DescendingTen(Num) {
  let number = [];
  if (Num == null) return (number = -1);
  let endNum = Num - 10;
  for (let i = Num; i > endNum; i--) {
    number.push(Num);
    Num = Num - 1;
  }
  return number;
}

console.log(DescendingTen(100)); // 100 99 98 97 96 95 94 93 92 91
console.log(DescendingTen(10)); // 10 9 8 7 6 5 4 3 2 1
console.log(DescendingTen()); // -1
