// 1. Animal Class Release 0
class Animal {
  constructor(Name) {
    this._name = Name;
    this._legs = 4;
    this._cold_blooded = false;
  }
  get name() {
    return this._name;
  }
  get legs() {
    return this._legs;
  }
  get cold_blooded() {
    return this._cold_blooded;
  }
}

var sheep = new Animal('shaun');

console.log(sheep.name); // "shaun"
console.log(sheep.legs); // 4
console.log(sheep.cold_blooded); // false

// 1. Animal Class Release 1
// Code class Ape dan class Frog di sini
class Ape extends Animal {
  constructor(Name) {
    super(Name);
    this._yell = 'Auooo';
  }
  yell() {
    return console.log(this._yell);
  }
}

class Frog extends Animal {
  constructor(Name) {
    super(Name);
    this._jump = 'hop hop';
  }
  jump() {
    return console.log(this._jump);
  }
}

var sungokong = new Ape('kera sakti');
sungokong.yell(); // "Auooo"

var kodok = new Frog('buduk');
kodok.jump(); // "hop hop"

// 2. Function to Class
class Clock {
  constructor({ template }) {
    this.template = template;
    this.timer;
  }

  render() {
    var date = new Date();

    var hours = date.getHours();
    if (hours < 10) hours = '0' + hours;

    var mins = date.getMinutes();
    if (mins < 10) mins = '0' + mins;

    var secs = date.getSeconds();
    if (secs < 10) secs = '0' + secs;

    var output = this.template
      .replace('h', hours)
      .replace('m', mins)
      .replace('s', secs);

    console.log(output);
  }
  stop() {
    clearInterval(this.timer);
  }
  start() {
    this.render();
    this.timer = setInterval(() => this.render(), 1000);
  }
}

var clock = new Clock({ template: 'h:m:s' });
clock.start();
