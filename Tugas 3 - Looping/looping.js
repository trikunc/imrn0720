// No. 1 Looping While
let x = 0;
console.log('LOOPING PERTAMA');
while (x < 20) {
  x = x + 2;
  console.log(x + ' - I love coding');
}
console.log('LOOPING KEDUA');
while (x > 0) {
  console.log(x + ' - I will become a mobile developer');
  x = x - 2;
}

// No. 2 Looping menggunakan for
for (let y = 1; y <= 20; y++) {
  if (y % 2 == 1 && y % 3 == 0) {
    console.log(y + ' - I Love Coding');
  } else if (y % 2 == 0) {
    console.log(y + ' - Berkualitas');
  } else {
    console.log(y + ' - Santai');
  }
}

// No. 3 Membuat Persegi Panjang #
let a = 4;
let b = 0;
while (a !== 0) {
  let output = '';
  while (b < 8) {
    output += '#';
    b++;
  }
  a--;
  b = 0;
  console.log(output);
}

// No. 4 Membuat Tangga
for (let i = 0; i < 7; i++) {
  let output = '';
  for (let j = 0; j <= i; j++) output += '#';
  console.log(output);
}

// No. 5 Membuat Papan Catur
let j;
for (let k = 1; k <= 8; k++) {
  let output = '';
  if (k % 2 == 1) j = 0;
  else j = 1;
  while (j < 8) {
    if (j % 2 == 1) output += '#';
    else output += ' ';
    j++;
  }
  console.log(output);
}
