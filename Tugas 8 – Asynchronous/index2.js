var readBooksPromise = require('./promise.js');

var books = [
  { name: 'LOTR', timeSpent: 3000 },
  { name: 'Fidas', timeSpent: 2000 },
  { name: 'Kalkulus', timeSpent: 4000 },
];

// Lanjutkan code untuk menjalankan function readBooksPromise

let time = 10000;
let n = 0;
let m = books.length;

function test() {
  let promise = readBooksPromise(time, books[n]);
  promise
    .then(function (resolve) {
      n = n + 1;
      time = resolve;
      if (n < m) {
        test();
      }
    })
    .catch(function (error) {
      console.log('selesai');
    });
}

test();
