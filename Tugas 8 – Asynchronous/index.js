// di index.js
var readBooks = require('./callback.js');

var books = [
  { name: 'LOTR', timeSpent: 3000 },
  { name: 'Fidas', timeSpent: 2000 },
  { name: 'Kalkulus', timeSpent: 4000 },
];

// Tulis code untuk memanggil function readBooks di sini

let time = 10000;
let n = 0;
let m = books.length;
function test() {
  readBooks(time, books[n], function (Y) {
    n = n + 1;
    time = Y;
    if (n < m) {
      test();
    }
  });
}
test();
