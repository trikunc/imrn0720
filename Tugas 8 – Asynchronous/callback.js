// di callback.js
function readBooks(time, book, callback) {
  console.log(`saya membaca ${book.name}`);
  // console.log(time);
  setTimeout(function () {
    let sisaWaktu = 0;
    if (time > book.timeSpent) {
      sisaWaktu = time - book.timeSpent;
      // console.log(time, book.timeSpent, sisaWaktu);
      console.log(
        `saya sudah membaca ${book.name}, sisa waktu saya ${sisaWaktu}`
      );
      callback(sisaWaktu); //menjalankan function callback
    } else {
      console.log('waktu saya habis');
      callback(time);
    }
  }, book.timeSpent);
}

module.exports = readBooks;
