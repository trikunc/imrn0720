// Soal No. 1 (Array to Object)
function arrayToObject(arr) {
  var now = new Date();
  var thisYear = now.getFullYear(); // 2020 (tahun sekarang)
  let objArr = {};
  let fullName;
  let Age;
  for (let i = 0; i < arr.length; i++) {
    let newArr = arr[i];
    for (let j = 0; j < newArr.length; j++) {
      fullName = newArr[0] + ' ' + newArr[1];
    }
    Age = thisYear - newArr[3];
    if (newArr[3] == null || Age <= 0) {
      Age = 'Invalid Birth Year';
    }

    objArr = {
      [`${fullName}`]: {
        firstName: newArr[0],
        lastName: newArr[1],
        gender: newArr[2],
        age: Age,
      },
    };
    console.log(objArr);
  }
}

// Driver Code
var people = [
  ['Bruce', 'Banner', 'male', 1975],
  ['Natasha', 'Romanoff', 'female'],
];
// var people = ["Bruce", "Banner", "male", 1975]
arrayToObject(people);
/*
  1. Bruce Banner: {
      firstName: "Bruce",
      lastName: "Banner",
      gender: "male",
      age: 45
  }
  2. Natasha Romanoff: {
      firstName: "Natasha",
      lastName: "Romanoff",
      gender: "female".
      age: "Invalid Birth Year"
  }
*/

var people2 = [
  ['Tony', 'Stark', 'male', 1980],
  ['Pepper', 'Pots', 'female', 2023],
];
arrayToObject(people2);
/*
  1. Tony Stark: {
      firstName: "Tony",
      lastName: "Stark",
      gender: "male",
      age: 40
  }
  2. Pepper Pots: {
      firstName: "Pepper",
      lastName: "Pots",
      gender: "female".
      age: "Invalid Birth Year"
  }
*/

// Error case
arrayToObject([]); // ""

// Soal No. 2 (Shopping Time)
function shoppingTime(memberId, money) {
  if (!memberId) return 'Mohon maaf, toko X hanya berlaku untuk member saja';
  if (money < 50000) return 'Mohon maaf, uang tidak cukup';
  let data;
  let newMoney = money;
  let purchased = [];

  data = {
    ['memberId']: memberId,
    ['money']: money,
  };
  if (money - 1500000 >= 0) {
    purchased.push('Sepatu Stacattu');
    newMoney = newMoney - 1500000;
  }
  if (money - 500000 >= 0) {
    purchased.push('Baju Zoro');
    newMoney = newMoney - 500000;
  }
  if (money - 250000 >= 0) {
    purchased.push('Baju H&N');
    newMoney = newMoney - 250000;
  }
  if (money - 175000 >= 0) {
    purchased.push('Sweater Uniklooh');
    newMoney = newMoney - 175000;
  }
  if (money - 50000 >= 0) {
    purchased.push('Casing Handphone');
    newMoney = newMoney - 50000;
  }
  data.listPurchased = purchased;
  data.changeMoney = newMoney;
  return data;
}

// TEST CASES
console.log(shoppingTime('1820RzKrnWn08', 2475000));
//{ memberId: '1820RzKrnWn08',
// money: 2475000,
// listPurchased:
//  [ 'Sepatu Stacattu',
//    'Baju Zoro',
//    'Baju H&N',
//    'Sweater Uniklooh',
//    'Casing Handphone' ],
// changeMoney: 0 }
console.log(shoppingTime('82Ku8Ma742', 170000));
//{ memberId: '82Ku8Ma742',
// money: 170000,
// listPurchased:
//  [ 'Casing Handphone' ],
// changeMoney: 120000 }
console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja

// Soal No. 3 (Naik Angkot)
function naikAngkot(arrPenumpang) {
  rute = ['A', 'B', 'C', 'D', 'E', 'F'];
  data = [];
  let k, l;

  for (let h = 0; h < arrPenumpang.length; h++) {
    let nweArrPenumpang = arrPenumpang[h];

    for (let i = 0; i <= rute.length; i++) {
      if (rute[i] == nweArrPenumpang[1]) {
        k = i;
      }
    }
    for (let j = 0; j <= rute.length; j++) {
      if (rute[j] == nweArrPenumpang[2]) {
        l = j;
      }
    }

    let obj = {
      penumpang: nweArrPenumpang[0],
      naikDari: nweArrPenumpang[1],
      tujuan: nweArrPenumpang[2],
      bayar: (l - k) * 2000,
    };
    data.push(obj);
  }
  arrPenumpang = data;
  return arrPenumpang;
}

//TEST CASE
console.log(
  naikAngkot([
    ['Dimitri', 'B', 'F'],
    ['Icha', 'A', 'B'],
  ])
);
// [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
//   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]

console.log(naikAngkot([])); //[]
